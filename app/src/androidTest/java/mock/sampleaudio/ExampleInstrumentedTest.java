package mock.sampleaudio;

import android.content.Context;
import android.media.AudioRecord;
import android.media.AudioTrack;
import android.media.MediaRecorder;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Test;
import org.junit.runner.RunWith;

import mock.sampleaudio.consts.PlayerConsts;
import mock.sampleaudio.fragments.player.PlayerPresenter;
import mock.sampleaudio.fragments.recorder.RecorderPresenter;

import static org.junit.Assert.*;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {
    @Test
    public void useAppContext() {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();

        assertEquals("mock.sampleaudio", appContext.getPackageName());
    }

    @Test
    public void test_recordingStateInitialise() {
        RecorderPresenter recorderPresenter = new RecorderPresenter();
        recorderPresenter.createRecorder();
        assertEquals(AudioRecord.STATE_INITIALIZED, recorderPresenter.getRecorderState());
        recorderPresenter.stopRecording();
    }

    @Test
    public void test_recorderSampleRate() {
        RecorderPresenter recorderPresenter = new RecorderPresenter();
        recorderPresenter.createRecorder();
        assertEquals(PlayerConsts.RECORDER_SAMPLERATE, recorderPresenter.getSamplingRate());
    }

    @Test
    public void test_recorderAudioSource() {
        RecorderPresenter recorderPresenter = new RecorderPresenter();
        recorderPresenter.createRecorder();
        assertEquals(MediaRecorder.AudioSource.MIC, recorderPresenter.getAudioSource());
    }

    @Test
    public void test_recordingStoppedState() {
        RecorderPresenter recorderPresenter = new RecorderPresenter();
        recorderPresenter.createRecorder();
        assertEquals(AudioRecord.RECORDSTATE_STOPPED, recorderPresenter.getRecordingState());
    }

    @Test
    public void test_recordingState() {
        RecorderPresenter recorderPresenter = new RecorderPresenter();
        recorderPresenter.startRecording();
        assertEquals(AudioRecord.RECORDSTATE_RECORDING, recorderPresenter.getRecordingState());
        recorderPresenter.stopRecording();
    }

    @Test
    public void test_recorderState() {
        RecorderPresenter recorderPresenter = new RecorderPresenter();
        recorderPresenter.startRecording();
        recorderPresenter.stopRecording();
        assertEquals(PlayerConsts.INVALID_STATE, recorderPresenter.getRecordingState());
    }

    @Test
    public void test_playerStateInitialise() {
        PlayerPresenter playerPresenter = new PlayerPresenter();
        assertNotEquals(null,playerPresenter.createAudioPlayer());
        assertEquals(AudioTrack.STATE_INITIALIZED, playerPresenter.createAudioPlayer().getState());
    }

    @Test
    public void test_playerPlayState() {
        PlayerPresenter playerPresenter = new PlayerPresenter();
        assertNotEquals(null,playerPresenter.createAudioPlayer());
        playerPresenter.startPlayback();
        assertEquals(AudioTrack.PLAYSTATE_PLAYING, playerPresenter.createAudioPlayer().getPlayState());
        playerPresenter.stopPlayback();
    }

    @Test
    public void test_playerPlayStoppedState() {
        PlayerPresenter playerPresenter = new PlayerPresenter();
        assertNotEquals(null,playerPresenter.createAudioPlayer());
        assertEquals(AudioTrack.PLAYSTATE_STOPPED, playerPresenter.createAudioPlayer().getPlayState());
    }
}
