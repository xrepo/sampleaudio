package mock.sampleaudio;

import org.junit.Test;

import mock.sampleaudio.fragments.recorder.RecorderPresenter;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {

    @Test
    public void test_recordingStateNotInitialise() {
        RecorderPresenter recorderPresenter = new RecorderPresenter();
        assertEquals(-1, recorderPresenter.getRecordingState());
    }
}