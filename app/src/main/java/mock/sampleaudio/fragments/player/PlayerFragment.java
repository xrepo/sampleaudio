package mock.sampleaudio.fragments.player;

import android.Manifest;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;

import mock.sampleaudio.customview.WaveformView;
import mock.sampleaudio.activities.MainActivity;
import mock.sampleaudio.R;
import mock.sampleaudio.consts.PlayerConsts;
import mock.sampleaudio.fragments.base.BaseFragment;
import mock.sampleaudio.listeners.IFragmentListener;
import mock.sampleaudio.listeners.IMainListener;

public class PlayerFragment extends BaseFragment implements IMainListener, PlayerPresenter.IPlayerView {
    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    private IFragmentListener iFragmentListener;

    private Button btnNextAction;
    private Button btnTimer;
    private ImageButton btnPlayerAction;

    private int time = 0;
    private Handler handler = new Handler();
    private PlayerPresenter playerPresenter;

    private WaveformView audioWaveView;
    private View view;
    private boolean isPlaying;

    public PlayerFragment() {
    }

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static PlayerFragment newInstance() {
        PlayerFragment fragment = new PlayerFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    private final Runnable playerHandler = new Runnable() {
        @Override
        public void run() {
            time = time + 1;
            btnTimer.setText(getString(R.string.timer_placeholder,time));
            if (time < PlayerConsts.MAX_RECORD_DURATION) {
                handler.postDelayed(playerHandler, PlayerConsts.TIMER_FREQUENCY);
            } else {
                handler.removeCallbacks(playerHandler);
                playerPresenter.stopPlayback();
                resetScreen();
            }
        }
    };

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        playerPresenter = new PlayerPresenter();
        playerPresenter.setListener(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.player_layout, container, false);
        view = rootView.findViewById(R.id.fragmentLayout);
        iFragmentListener = (MainActivity) getActivity();
        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        audioWaveView = view.findViewById(R.id.wave);
        btnPlayerAction = view.findViewById(R.id.btnPlayerAction);
        btnPlayerAction.setImageResource(R.drawable.ic_media_play);
        btnPlayerAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startPlayback();
            }
        });

        btnTimer = view.findViewById(R.id.btnTimer);

        btnNextAction = view.findViewById(R.id.btnNextAction);
        btnNextAction.setText(getString(R.string.btn_back_to_record));
        btnTimer.setMaxWidth(btnNextAction.getLayoutParams().width);

        btnNextAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iFragmentListener.onFragmentUpdate(PlayerConsts.STATE_VIEW_CHANGE);
            }
        });
    }

    private void startPlayback() {
        if (checkIfPermissionGranted(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            playerPresenter.startPlayback();
            if (!isPlaying) {
                audioWaveView.initializeWaveform(0);
            }
            isPlaying = true; // ToDo should come from presenter
        } else {
            checkForPermission(Manifest.permission.READ_EXTERNAL_STORAGE);
        }
    }


    private void resetScreen() {
        time = 0;
        btnTimer.setText(R.string.title_player_screen);
        btnPlayerAction.setImageResource(R.drawable.ic_media_play);
        handler.removeCallbacks(playerHandler);
        isPlaying = false;
    }

    public void onFinish() {
        resetScreen();
        playerPresenter.stopPlayback();
    }

    @Override
    public void onPlayerCommand(int cmd) {
        switch (cmd) {
            case PlayerConsts.STATE_PLAYBACK_STARTING:
                if (time == 0) {
                    btnTimer.setText(getString(R.string.timer_placeholder,time));
                }
                btnPlayerAction.setImageResource(R.drawable.ic_media_pause);
                handler.postDelayed(playerHandler, PlayerConsts.TIMER_FREQUENCY);
                break;
            case PlayerConsts.INVALID_FILE_OPERATION:
                Snackbar.make(view,getString(R.string.error_invalid_file_operation),Snackbar.LENGTH_SHORT).show();
                break;
            case PlayerConsts.STATE_PLAYBACK_STOPPED:
                resetScreen();
                break;
            case PlayerConsts.STATE_PLAYBACK_IN_PROGRESS:
                Snackbar.make(view,getString(R.string.msg_playback_progress), Snackbar.LENGTH_SHORT).show();
                break;
        }
    }

    @Override
    public void onPermissionUpdate(int cmd) {
        switch (cmd) {
            case PlayerConsts.STATE_USER_CANCELLED_PERMISSION_DIALOG:
                iFragmentListener.onFragmentUpdate(PlayerConsts.STATE_EXIT);
                break;
            case PlayerConsts.ERROR_LAUNCHING_SETTINGS:
                Snackbar.make(view, getString(R.string.msg_open_settings_manually),Snackbar.LENGTH_SHORT).show();
                 break;
            case PlayerConsts.AUDIO_RECORD_PERMISSION_DENIED:
            case PlayerConsts.STORAGE_PERMISSION_DENIED:
                String permissionType = PlayerConsts.MICROPHONE;
                if (cmd == PlayerConsts.STORAGE_PERMISSION_DENIED){
                    permissionType = PlayerConsts.STORAGE;
                }
                Snackbar.make(view, getString(R.string.error_permission_denied, permissionType),Snackbar.LENGTH_SHORT)
                        .setAction(getString(R.string.title_settings), new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                launchSettings();
                            }
                        })
                        .show();
                break;
        }
    }


    @Override
    public void onAmplitudeUpdate(final float amplitude) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                audioWaveView.addAmplitude(amplitude);
            }
        });
    }
}
