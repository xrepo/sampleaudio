package mock.sampleaudio.fragments.player;


import android.media.AudioAttributes;
import android.media.AudioFormat;
import android.media.AudioTrack;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import mock.sampleaudio.consts.PlayerConsts;
import mock.sampleaudio.utils.PlayerUtil;

public class PlayerPresenter {
    private Thread playingThread = null;
    private AudioTrack audioPlayer;
    private byte[] byteData = null;
    private int count = 16 * 1024;
    private int bytesread = 0;
    private int ret = 0;
    private int size;
    private FileInputStream in = null;
    private boolean isPlay = false;

    private IPlayerView iPlayerView;

    public PlayerPresenter() {

    }

    public void setListener(IPlayerView playerView) {
        iPlayerView = playerView;
    }

    public void startPlayback() {
        if (isPlay) {
            updateView(PlayerConsts.STATE_PLAYBACK_IN_PROGRESS);
            return;
        }

        if (audioPlayer == null) {
            createAudioPlayer();
        }
        updateView(PlayerConsts.STATE_PLAYBACK_STARTING);

        isPlay = true;
        bytesread = 0;
        ret = 0;
        File file = null;
        if (PlayerConsts.AUDIO_FILE == null) {
            return;
        } else {
            file = new File(PlayerConsts.AUDIO_FILE);
            try {
                in = new FileInputStream(file);
            } catch (FileNotFoundException e) {
                updateView(1);
                return;
            }
        }

        size = (int) file.length();
        byteData = new byte[count];

        audioPlayer.play();

        playingThread = new Thread(new Runnable() {
            public void run() {
                playFromFile();
            }
        }, "Player Thread");
        playingThread.start();
    }

    public void playFromFile() {
        while (bytesread < size && isPlay) {
            if (Thread.currentThread().isInterrupted()) {
                break;
            }
            try {
                ret = in.read(byteData, 0, count);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (ret != -1) { // Write the byte array to the track
                updateAmplitude(byteData);
                audioPlayer.write(byteData,0, ret);
                bytesread += ret;
            } else break;
        }
        try {
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (audioPlayer != null){
            if (audioPlayer.getState()!=AudioTrack.PLAYSTATE_STOPPED){
                updateView(PlayerConsts.STATE_PLAYBACK_STOPPED);
                audioPlayer.stop();
                audioPlayer.release();
                isPlay = false;
                playingThread = null;
            }
        }
    }

    public AudioTrack createAudioPlayer(){
        int playBufferSize = 2 * android.media.AudioTrack.getMinBufferSize(PlayerConsts.RECORDER_SAMPLERATE, AudioFormat.CHANNEL_OUT_STEREO,
                AudioFormat.ENCODING_PCM_16BIT);
        AudioAttributes audioAttributes = new AudioAttributes.Builder()
                .setUsage(AudioAttributes.USAGE_MEDIA)
                .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                .build();

        AudioFormat audioFormat = new AudioFormat.Builder()
                .setChannelMask(AudioFormat.CHANNEL_OUT_STEREO)
                .build();

        if (audioPlayer == null) {
            audioPlayer = new AudioTrack(audioAttributes,
                    audioFormat,
                    playBufferSize,
                    AudioTrack.MODE_STREAM,
                    1);
            audioPlayer.setPlaybackRate(PlayerConsts.RECORDER_SAMPLERATE);
        }
        return  audioPlayer;
    }

    public void stopPlayback(){
        isPlay = false;
        if (playingThread != null) {
            playingThread.interrupt();
            playingThread = null;
        }
        if (audioPlayer != null) {
            audioPlayer.stop();
            audioPlayer.release();
            audioPlayer = null;
        }
    }

    private void updateAmplitude(final byte[] bData) {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                short[] rawData = PlayerUtil.byteToShort(bData);
                iPlayerView.onAmplitudeUpdate(PlayerUtil.getMaxAmplitude(rawData));
            }
        });
        thread.start();
    }

    private void updateView(int cmd) {
        if (null != iPlayerView) {
            iPlayerView.onPlayerCommand(cmd);
        }
    }

    interface IPlayerView {
        void onPlayerCommand(int cmd);
        void onAmplitudeUpdate(float amplitude);
    }
}
