package mock.sampleaudio.fragments.recorder;

import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder;
import android.os.Build;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;

import mock.sampleaudio.consts.PlayerConsts;
import mock.sampleaudio.utils.PlayerUtil;

public class RecorderPresenter {

    private IRecorderView iRecorderView;

    private AudioRecord recorder = null;
    private Thread recordingThread = null;
    private boolean isRecording = false;
    private int readBufferSize;

    public RecorderPresenter() {

    }

    public void setListener(IRecorderView recorderView) {
        iRecorderView = recorderView;
    }

    public void createRecorder() {
        readBufferSize = 2 * AudioRecord.getMinBufferSize(PlayerConsts.RECORDER_SAMPLERATE,
                PlayerConsts.RECORDER_CHANNELS, PlayerConsts.RECORDER_AUDIO_ENCODING);

        if (recorder == null) {
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                recorder = new AudioRecord.Builder()
                        .setAudioSource(MediaRecorder.AudioSource.MIC)
                        .setAudioFormat(new AudioFormat.Builder()
                                .setEncoding(AudioFormat.ENCODING_PCM_16BIT)
                                .setSampleRate(PlayerConsts.RECORDER_SAMPLERATE)
                                .setChannelMask(AudioFormat.CHANNEL_IN_STEREO)
                                .build())
                        .setBufferSizeInBytes(readBufferSize)
                        .build();
            } else {
                recorder = new AudioRecord(MediaRecorder.AudioSource.MIC,
                        PlayerConsts.RECORDER_SAMPLERATE, PlayerConsts.RECORDER_CHANNELS,
                        PlayerConsts.RECORDER_AUDIO_ENCODING, readBufferSize);
            }
        }
    }

    public void startRecording() {
        if (isRecording) {
            updateView(PlayerConsts.STATE_RECORDING_IN_PROGRESS);
            return;
        }
        updateView(PlayerConsts.STATE_RECORDING_STARTING);
        createRecorder();
        if (recorder.getState() != AudioRecord.STATE_INITIALIZED) {
            return;
        }

        recorder.startRecording();
        isRecording = true;
        recordingThread = new Thread(new Runnable() {
            @Override
            public void run() {
                writeAudioDataToFile();
            }
        },"Recorder Thread");
        recordingThread.start();
    }

    private void writeAudioDataToFile() {
        // Write the output audio in byte
        final byte[] bData = new byte[readBufferSize];
        FileOutputStream os = null;
        try {
            os = new FileOutputStream(PlayerConsts.AUDIO_FILE);
            while (isRecording) {
                // gets the voice output from microphone to byte format
                recorder.read(bData,0, readBufferSize);
                //writes the data to file from buffer
                updateAmplitude(bData);
                os.write(bData, 0, bData.length);
            }
            os.close();
        } catch (FileNotFoundException e) {
            invalidFileOperation();
        } catch (IOException e) {
            invalidFileOperation();
        }
    }

    private void updateAmplitude(final byte[] bData) {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                short[] rawData = PlayerUtil.byteToShort(bData);
                iRecorderView.onAmplitudeUpdate(PlayerUtil.getMaxAmplitude(rawData));
            }
        });
        thread.start();
    }

    private void invalidFileOperation() {
        stopRecording();
        updateView(PlayerConsts.INVALID_FILE_OPERATION);
    }

    public void stopRecording() {
        // stops the recording
        if (null != recorder) {
            isRecording = false;
            recorder.stop();
            recorder.release();
            recorder = null;
            recordingThread = null;
        }
    }

    public int getRecordingState() {
        int state = PlayerConsts.INVALID_STATE;
        if (recorder != null) {
            state = recorder.getRecordingState();
        }
        return state;
    }

    public int getRecorderState() {
        int state = PlayerConsts.INVALID_STATE;
        if (recorder != null) {
            state = recorder.getState();
        }
        return state;
    }

    public int getSamplingRate() {
        return recorder.getSampleRate();
    }

    public int getAudioSource() {
        return recorder.getAudioSource();
    }

    private void updateView(int cmd) {
        if (null != iRecorderView) {
            iRecorderView.onRecorderCommand(cmd);
        }
    }

    public interface IRecorderView {
        void onRecorderCommand(int cmd);
        void onAmplitudeUpdate(float amplitude);
    }
}
