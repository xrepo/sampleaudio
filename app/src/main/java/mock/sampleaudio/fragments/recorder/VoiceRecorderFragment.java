package mock.sampleaudio.fragments.recorder;

import android.Manifest;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;

import mock.sampleaudio.customview.WaveformView;
import mock.sampleaudio.activities.MainActivity;
import mock.sampleaudio.R;
import mock.sampleaudio.consts.PlayerConsts;
import mock.sampleaudio.fragments.base.BaseFragment;
import mock.sampleaudio.listeners.IFragmentListener;
import mock.sampleaudio.listeners.IMainListener;

public class VoiceRecorderFragment extends BaseFragment implements IMainListener, RecorderPresenter.IRecorderView {
    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    private Button btnNextAction;
    private Button btnTimer;
    private int time = 0;
    private Handler handler = new Handler();
    private IFragmentListener iFragmentListener;
    private ImageButton btnPlayerAction;

    private RecorderPresenter recorderPresenter;
    private WaveformView audioWaveView;
    private boolean isRecording;

    private View view;

    public VoiceRecorderFragment() {
    }

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static VoiceRecorderFragment newInstance() {
        VoiceRecorderFragment fragment = new VoiceRecorderFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    private final Runnable recordHandler = new Runnable() {
        @Override
        public void run() {
            time = time + 1;
            btnTimer.setText(getString(R.string.timer_placeholder,time));
            if (time%2 == 0) {
                //audioWaveView.setWaveColor(Color.BLUE);
            } else {
                //audioWaveView.setWaveColor(Color.GREEN);
            }
            if (time < PlayerConsts.MAX_RECORD_DURATION) {
                handler.postDelayed(recordHandler, PlayerConsts.TIMER_FREQUENCY);
            } else {
                recorderPresenter.stopRecording();
                resetScreen();
            }
        }
    };

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        recorderPresenter = new RecorderPresenter();
        recorderPresenter.setListener(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.recorder_layout, container, false);
        view = rootView.findViewById(R.id.fragmentLayout);
        iFragmentListener = (MainActivity)getActivity();
        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        btnPlayerAction = view.findViewById(R.id.btnPlayerAction);
        btnPlayerAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnNextAction.setEnabled(false);
                ((GradientDrawable)btnNextAction.getBackground()).setStroke(2, getResources().getColor(R.color.playerLGrey));
                startRecording();
            }
        });

        btnTimer = view.findViewById(R.id.btnTimer);

        audioWaveView = view.findViewById(R.id.wave);

        btnNextAction = view.findViewById(R.id.btnNextAction);
        btnTimer.setMaxWidth(btnNextAction.getLayoutParams().width);
        btnNextAction.setEnabled(false);

        btnNextAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iFragmentListener.onFragmentUpdate(PlayerConsts.STATE_VIEW_CHANGE);
            }
        });

        ((GradientDrawable)btnNextAction.getBackground()).setStroke(2, getResources().getColor(R.color.playerLGrey));
    }

    private void resetScreen() {
        time = 0;
        btnTimer.setText(R.string.timer_text);
        btnPlayerAction.setImageResource(R.drawable.ic_media_record);
        handler.removeCallbacks(recordHandler);
        isRecording = false;
        btnNextAction.setEnabled(true);
        ((GradientDrawable)btnNextAction.getBackground()).setStroke(3, getResources().getColor(R.color.playerBlue));
    }

    @Override
    public void onFinish() {
        resetScreen();
        recorderPresenter.stopRecording();
    }

    private void startRecording() {
        if (checkIfPermissionGranted(Manifest.permission.RECORD_AUDIO) &&
                checkIfPermissionGranted(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            recorderPresenter.startRecording();
            if (!isRecording) {
                audioWaveView.initializeWaveform(0);
            }
            isRecording = true; //ToDo should come from presenter
        } else {
            String permission = Manifest.permission.RECORD_AUDIO;
            if(!checkIfPermissionGranted(Manifest.permission.RECORD_AUDIO)) {
                permission = Manifest.permission.RECORD_AUDIO;
            } else if(!checkIfPermissionGranted(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                permission = Manifest.permission.WRITE_EXTERNAL_STORAGE;
            }
            checkForPermission(permission);
        }
    }

    @Override
    public void onRecorderCommand(int cmd) {
        switch (cmd) {
            case PlayerConsts.STATE_RECORDING_STARTING:
                if (time == 0) {
                    btnTimer.setText(getString(R.string.timer_placeholder,time));
                    btnPlayerAction.setImageResource(R.drawable.ic_media_stop);
                }
                handler.postDelayed(recordHandler, PlayerConsts.TIMER_FREQUENCY);
                break;
            case PlayerConsts.INVALID_FILE_OPERATION:
                Snackbar.make(view,getString(R.string.error_invalid_file_operation),Snackbar.LENGTH_SHORT).show();
                break;
            case PlayerConsts.STATE_RECORDING_IN_PROGRESS:
                Snackbar.make(view,getString(R.string.msg_recording_progress), Snackbar.LENGTH_SHORT).show();
                break;
            default:

        }
    }

    @Override
    public void onPermissionUpdate(int cmd) {
        switch (cmd) {
            case PlayerConsts.STATE_USER_CANCELLED_PERMISSION_DIALOG:
                iFragmentListener.onFragmentUpdate(PlayerConsts.STATE_EXIT);
                break;
            case PlayerConsts.ERROR_LAUNCHING_SETTINGS:
                Snackbar.make(view, getString(R.string.msg_open_settings_manually),Snackbar.LENGTH_SHORT).show();
                break;
            case PlayerConsts.AUDIO_RECORD_PERMISSION_DENIED:
            case PlayerConsts.STORAGE_PERMISSION_DENIED:
                String permissionType = PlayerConsts.MICROPHONE;
                if (cmd == PlayerConsts.STORAGE_PERMISSION_DENIED){
                    permissionType = PlayerConsts.STORAGE;
                }
                Snackbar.make(view,getString(R.string.error_permission_denied, permissionType),Snackbar.LENGTH_SHORT)
                        .setAction(getString(R.string.title_settings), new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                launchSettings();
                            }
                        })
                        .show();
                break;
        }
    }

    @Override
    public void onAmplitudeUpdate(final float amplitude) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                audioWaveView.addAmplitude(amplitude);
            }
        });
    }
}
