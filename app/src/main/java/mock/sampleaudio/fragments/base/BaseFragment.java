package mock.sampleaudio.fragments.base;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;

import mock.sampleaudio.R;
import mock.sampleaudio.consts.PlayerConsts;

public abstract class BaseFragment extends Fragment {
    private static final String SCHEME_PACKAGE = "package";
    public abstract void onPermissionUpdate(int cmd);

    protected void launchSettings() {
        try {
            Intent intent = new Intent();
            intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
            Uri uri = Uri.fromParts(SCHEME_PACKAGE, getActivity().getPackageName(), null);
            intent.setData(uri);
            startActivity(intent);
        } catch (Exception e) {
            onPermissionUpdate(PlayerConsts.ERROR_LAUNCHING_SETTINGS);
        }
    }

    protected boolean checkIfPermissionGranted(String permission) {
        return ContextCompat.checkSelfPermission(getContext(),
                permission) == PackageManager.PERMISSION_GRANTED;
    }

    protected void checkForPermission(final String permission) {
        int reqCode = PlayerConsts.NONE;
        if (Manifest.permission.RECORD_AUDIO.equals(permission)) {
            reqCode = PlayerConsts.REQUEST_CODE_AUDIO_RECORD_PERMISSION;
        } else if (Manifest.permission.WRITE_EXTERNAL_STORAGE.equals(permission) ||
                Manifest.permission.READ_EXTERNAL_STORAGE.equals(permission)) {
            reqCode = PlayerConsts.REQUEST_CODE_STORAGE_PERMISSION;
        }

        if (ContextCompat.checkSelfPermission(getActivity(),
                permission)
                != PackageManager.PERMISSION_GRANTED) {

            // Permission is not granted
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                    permission)) {
                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                showPermissionAlert(permission, reqCode);
            } else {
                // No explanation needed; request the permission
                requestPermissions(new String[]{permission},
                        reqCode);

            }
        } else {
            // Permission has already been granted
            onPermissionUpdate(PlayerConsts.PERMISSION_GRANTED);
        }
    }

    protected void showPermissionAlert(final String permission, final int reqCode) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(getString(R.string.dlg_permission_denied_text, permission))
                .setPositiveButton(getString(R.string.dlg_ok_button), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        requestPermissions(new String[]{permission},
                                reqCode);
                    }
                })
                .setNegativeButton(getString(R.string.dlg_cancel_button), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                        onPermissionUpdate(PlayerConsts.STATE_USER_CANCELLED_PERMISSION_DIALOG);
                    }
                });
        builder.create().show();
    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           final String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case PlayerConsts.REQUEST_CODE_AUDIO_RECORD_PERMISSION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay!
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    onPermissionUpdate(PlayerConsts.AUDIO_RECORD_PERMISSION_DENIED);
                }
                return;
            }

            case PlayerConsts.REQUEST_CODE_STORAGE_PERMISSION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay!
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    onPermissionUpdate(PlayerConsts.STORAGE_PERMISSION_DENIED);
                }
                return;
            }

            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }
}
