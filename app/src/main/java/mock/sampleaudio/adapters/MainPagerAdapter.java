package mock.sampleaudio.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

import mock.sampleaudio.listeners.IMainListener;

public class MainPagerAdapter extends FragmentPagerAdapter {

    private final List<IMainListener> fragmentList;
    private final List<String> fragmentTitleList;

    public MainPagerAdapter(FragmentManager fm) {
        super(fm);
        fragmentList = new ArrayList<>();
        fragmentTitleList = new ArrayList<>();
    }

    public void addFragment(IMainListener fragment, String title) {
        fragmentList.add(fragment);
        fragmentTitleList.add(title);
    }

    public void sendEvent(int index) {
        fragmentList.get(index).onFinish();
    }

    @Override
    public Fragment getItem(int position) {
        return (Fragment) fragmentList.get(position);
    }


    @Override
    public int getCount() {
        return fragmentList.size();
    }


    @Override
    public CharSequence getPageTitle(int position) {
        return fragmentTitleList.get(position);
    }
}
