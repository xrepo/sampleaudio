package mock.sampleaudio.utils;

import android.content.Context;

import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class PlayerUtil {

    public static synchronized short[] byteToShort(byte[] raw) {
        int len = (raw.length%2==0)?raw.length/2:raw.length/2+1;
        short[] convert = new short[len];
        int j = 0;
        for(int i = 0;i < raw.length-1; i+=2) {
            convert[j++] = (short)((raw[i] & 0xFF) | raw[i+1] << 8);
        }
        return convert;
    }

    public static synchronized float getMaxAmplitude(final short[] rawData) {
        int maxAmplitude = 0;

        for (short amplitude : rawData) {
            if (maxAmplitude < Math.abs(amplitude)) {
                maxAmplitude = amplitude;
            }
        }
        return maxAmplitude;
    }

    /**
     * unused method, kept for future use
     */
    public static synchronized byte[] shortToByte(short[] data) {
        byte[] bData = new byte[2 * data.length];
        int j = 0;
        for(int i = 0;i < data.length;i++) {
            bData[j] = (byte)(data[i] & 0x00FF);
            bData[j+1] = (byte)((data[i]) >> 8);
            j+=2;
        }
        return bData;
    }

    // Format time (hh:mm:ss).
    public static String formatTime(long duration) {
        String hms;
        if (TimeUnit.SECONDS.toHours(duration) > 0)
            hms = String.format(Locale.getDefault(), "%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(duration),
                    TimeUnit.SECONDS.toMinutes(duration) % TimeUnit.HOURS.toMinutes(1),
                    TimeUnit.SECONDS.toSeconds(duration) % TimeUnit.MINUTES.toSeconds(1));
        else
            hms = String.format(Locale.getDefault(), "%02d:%02d", TimeUnit.SECONDS.toMinutes(duration) % TimeUnit.HOURS.toMinutes(1),
                    TimeUnit.SECONDS.toSeconds(duration) % TimeUnit.MINUTES.toSeconds(1));

        return hms;
    }

    public static int dpToPixel(Context context, float dp) {
        return (int) (dp * context.getResources().getDisplayMetrics().density + 0.5f);
    }
}
