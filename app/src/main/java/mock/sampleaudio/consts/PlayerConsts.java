package mock.sampleaudio.consts;

import android.media.AudioFormat;
import android.os.Environment;

public class PlayerConsts {

    public static final int RECORDER_SAMPLERATE = 44100;
    public static final int RECORDER_CHANNELS = AudioFormat.CHANNEL_IN_STEREO;
    public static final int RECORDER_AUDIO_ENCODING = AudioFormat.ENCODING_PCM_16BIT;

    private static String filePath = Environment.getExternalStorageDirectory().getPath()+"/";
    private static String fileName = "sample44100.pcm";
    public static final String AUDIO_FILE = filePath + fileName;

    //string consts
    public static final String MICROPHONE = "Mic";
    public static final String STORAGE = "Storage";

    //error consts
    public static final int INVALID_STATE = -1;
    public static final int INVALID_FILE_OPERATION = -2;

    //state
    public static final int STATE_EXIT = 0;
    public static final int AUDIO_RECORD_PERMISSION_DENIED = 2;
    public static final int STORAGE_PERMISSION_DENIED = 3;

    public static final int STATE_PLAYBACK_STARTING = 4;
    public static final int STATE_PLAYBACK_IN_PROGRESS = 5;
    public static final int STATE_RECORDING_STARTING = 6;
    public static final int STATE_RECORDING_IN_PROGRESS = 7;
    public static final int STATE_PLAYBACK_STOPPED = 8;
    public static final int STATE_VIEW_CHANGE = 9;

    //request codes, permission
    public static final int STATE_USER_CANCELLED_PERMISSION_DIALOG = -1;
    public static final int ERROR_LAUNCHING_SETTINGS = 0;
    public static final int PERMISSION_GRANTED = 1;
    public static final int REQUEST_CODE_AUDIO_RECORD_PERMISSION = 200;
    public static final int REQUEST_CODE_STORAGE_PERMISSION = 201;

    // general
    public static int MAX_RECORD_DURATION = 20;//20 secs
    public static int TIMER_FREQUENCY = 950;//in ms
    public static int NONE = 0;
}
