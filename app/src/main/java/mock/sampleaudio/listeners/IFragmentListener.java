package mock.sampleaudio.listeners;

public interface IFragmentListener {
    void onFragmentUpdate(int cmd);
}
