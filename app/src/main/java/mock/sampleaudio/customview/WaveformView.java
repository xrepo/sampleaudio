package mock.sampleaudio.customview;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import mock.sampleaudio.R;
import mock.sampleaudio.utils.PlayerUtil;

public class WaveformView extends View {
    private static final int LINE_WIDTH_DP = 1;
    private static final int LINES_SPACE = 1;
    private static final int MAX_AMPLITUDE = 32767;
    private static final int MIN_AMPLITUDE = 100;
    private static final int TOP_PADDING = 12;
    private static final int WIDTH_BETWEEN_MARKERS = 32;

    private List<Float> amplitudes = new ArrayList<>(); // amplitudes
    private boolean isRecording;
    private int startTime;
    private int start;
    private int secOnLongMark;
    private int width;
    private int height;
    private float startHeight;
    private float centralHeight;
    private float markAmplitude;
    private float startGrid;
    private float timeScaleWidth;
    private Paint paintAmplitude;
    private Paint paintPlaybackIndicator;
    private Paint paintPlayer;
    private Paint paintShortMark;
    private Paint paintLongMark;
    private Paint paintPlaybackLine;
    private Path playbackLine;
    private Paint paintTimeMark;
    private Rect rectPlayer;

    public WaveformView(Context context) {
        super(context);
    }

    public WaveformView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();

        setup();
    }

    // waveform view setup
    private void setup() {
        float scale = getResources().getDisplayMetrics().density;

        // Red line indicating the current position.
        paintPlaybackIndicator = new Paint();
        paintPlaybackIndicator.setColor(getResources().getColor(R.color.playerRed));
        paintPlaybackIndicator.setStrokeWidth(markAmplitude);

        // player view boundary.
        paintPlayer = new Paint();
        paintPlayer.setColor(getResources().getColor(R.color.playerVeryLightGrey));
        paintPlayer.setStyle(Paint.Style.FILL);

        // Grid.
        startGrid = 0; // the grid is drawn starting from x = 0

        // playback time marks
        secOnLongMark = 5;
        paintTimeMark = new Paint();
        paintTimeMark.setTextSize(8 * scale);
        paintTimeMark.setColor(getResources().getColor(R.color.playerBlue));
        paintTimeMark.setAntiAlias(true);
        paintTimeMark.setTextAlign(Paint.Align.CENTER);

        // Long time marks.
        paintLongMark = new Paint();
        paintLongMark.setColor(getResources().getColor(R.color.playerDarkGrey));
        paintLongMark.setStrokeWidth(markAmplitude);

        // Short time marks.
        paintShortMark = new Paint();
        paintShortMark.setColor(getResources().getColor(R.color.playerLGrey));
        paintShortMark.setStrokeWidth(markAmplitude);

        // Playback line
        playbackLine = new Path();
        paintPlaybackLine = new Paint();
        paintPlaybackLine.setColor(Color.LTGRAY);
        paintPlaybackLine.setStrokeWidth(markAmplitude);
        paintPlaybackLine.setStyle(Paint.Style.STROKE);

        // Lines of amplitudes.
        markAmplitude = PlayerUtil.dpToPixel(getContext(),LINE_WIDTH_DP);
        paintAmplitude = new Paint();
        paintAmplitude.setColor(getResources().getColor(R.color.playerBlue));
        paintAmplitude.setStrokeWidth(markAmplitude);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldWidth, int oldHeight) {
        amplitudes = new ArrayList<>();

        width = w;
        height = h;
        startHeight = PlayerUtil.dpToPixel(getContext(),TOP_PADDING); // padding at the top to show times
        centralHeight = height * 0.5f;
        timeScaleWidth = width;
        rectPlayer = new Rect(0, (int) startHeight, width, height);
    }

    // Start and stop recording.
    public void initializeWaveform(int secondsElapsed) {
        setup();
        amplitudes.clear();
        startTime = secondsElapsed;
        start = startTime;
        isRecording = true;
        invalidate();
    }

    public void resetWaveform() {
        setup();
        amplitudes.clear();
        startTime = 0;
        start = startTime;
        isRecording = false;
        invalidate();
    }

    // Add the given amplitude to the amplitudes ArrayList.
    public void addAmplitude(float amplitude) {
        amplitude = Math.min((float) (amplitude * 1.2), MAX_AMPLITUDE); // increase sensitivity
        amplitude = amplitude < MIN_AMPLITUDE ? MIN_AMPLITUDE : amplitude + MIN_AMPLITUDE;

        amplitudes.add(amplitude);
        invalidate();
    }

    // Draw the visualizer with scaled lines representing the amplitudes.
    @Override
    public void onDraw(Canvas canvas) {
        drawBox(canvas);

        if (isRecording) {
            drawAmplitudes(canvas);
        }
    }

    // Draw the the box.
    private void drawBox(Canvas canvas) {
        // player boundary
        canvas.drawRect(rectPlayer, paintPlayer);

        // time markers
        int count = 0;
        int time;
        for (float x = startGrid; x < timeScaleWidth; x += WIDTH_BETWEEN_MARKERS, count++) {
            if (count % 5 == 0) { // long mark
                canvas.drawLine(x, startHeight, x, height / 5, paintShortMark);
                time = start + (count / 5) * secOnLongMark;
                if (count > 0 || start > startTime)
                    canvas.drawText(PlayerUtil.formatTime(time), x, startHeight - PlayerUtil.dpToPixel(getContext(),4), paintTimeMark);
            } else { // short mark
                canvas.drawLine(x, startHeight, x, height / 7, paintLongMark);
            }
        }

        // playback line
        playbackLine.moveTo(0, centralHeight);
        playbackLine.quadTo(width/2, centralHeight, width, centralHeight);
        canvas.drawPath(playbackLine, paintPlaybackLine);
    }

    private void drawAmplitudes(Canvas canvas) {
        // draw amplitudes
        float curX = 0; // the x position where to draw the lines
        for (float amplitude : amplitudes) {
            float scaledHeight = (float) ((amplitude / MAX_AMPLITUDE) * centralHeight * 0.50);
            curX += markAmplitude * LINES_SPACE;
            canvas.drawLine(curX, centralHeight + scaledHeight, curX, centralHeight
                    - scaledHeight, paintAmplitude);
        }

        // Draw playback indicator
        if (amplitudes.size() > 0) {
            curX += markAmplitude * LINES_SPACE;
            canvas.drawLine(curX, startHeight, curX, height, paintPlaybackIndicator);
        }
    }
}
