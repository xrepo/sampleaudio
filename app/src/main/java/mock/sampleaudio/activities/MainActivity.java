package mock.sampleaudio.activities;

import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;

import mock.sampleaudio.R;
import mock.sampleaudio.adapters.MainPagerAdapter;
import mock.sampleaudio.consts.PlayerConsts;
import mock.sampleaudio.fragments.player.PlayerFragment;
import mock.sampleaudio.fragments.recorder.VoiceRecorderFragment;
import mock.sampleaudio.listeners.IFragmentListener;

public class MainActivity extends AppCompatActivity implements IFragmentListener {

    private MainPagerAdapter mainPagerAdapter;
    private ViewPager viewPager;
    private boolean enableSwipe;
    private final int RECORD_VIEW = 0;
    private final int PLAYER_VIEW = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // viewpager setup
        viewPager = findViewById(R.id.container);
        mainPagerAdapter = new MainPagerAdapter(getSupportFragmentManager());
        mainPagerAdapter.addFragment(VoiceRecorderFragment.newInstance(), "");
        mainPagerAdapter.addFragment(PlayerFragment.newInstance(),"");
        viewPager.setAdapter(mainPagerAdapter);

        ((View) viewPager).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return enableSwipe;
            }
        });

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {
                mainPagerAdapter.sendEvent((i == RECORD_VIEW) ? PLAYER_VIEW : RECORD_VIEW);
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });

    }

    @Override
    public void onFragmentUpdate(int cmd) {
        switch (cmd) {
            case PlayerConsts.STATE_EXIT:
                finish();
                break;
            case PlayerConsts.STATE_VIEW_CHANGE:
                viewPager.setCurrentItem((viewPager.getCurrentItem() == RECORD_VIEW) ? PLAYER_VIEW : RECORD_VIEW);
                break;
        }
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy() {
        mainPagerAdapter.sendEvent(viewPager.getCurrentItem());
        super.onDestroy();
    }
}
